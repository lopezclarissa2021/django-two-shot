from django.urls import path
from accounts.views import login_form, signup, user_logout

urlpatterns = [
  path('login/', login_form, name='login'),
  path('logout/', user_logout, name='logout'),
  path('signup/', signup, name='signup'),
]