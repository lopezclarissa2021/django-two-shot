from django import forms
from django.forms import ModelForm
from receipts.models import Account, ExpenseCategory, Receipt

class ReceiptForm(ModelForm):
    class Meta:
      model = Receipt
      fields = [
        'vendor',
        'total',
        'tax',
        'category',
        'account',
        'date'
      ]
      def __init__(self, *args, owner=None, **kwargs):
        super().__init__(*args, **kwargs)
        if owner:
            self.fields['category'].queryset = owner.categories.all()
            self.fields['account'].queryset = owner.accounts.all()

class ExpenseCategoryForm(ModelForm):
    class Meta:
      model = ExpenseCategory
      fields = [
        'name',
      ]

class AccountForm(ModelForm):
    class Meta:
      model = Account
      fields = [
        'name',
        'number'
      ]