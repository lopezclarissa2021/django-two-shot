from django.urls import path
from receipts.views import create_account, create_category, create_receipt, date_order, reciept_list, category_list, account_list, create_account, total_order, vendor_order, category_order, account_order


urlpatterns = [
  #home path:
  path('', reciept_list, name='home'),
  # oder_by paths:
  path('vendor/', vendor_order, name='vendor_order'),
  path('date/', date_order, name='date_order'),
  path('category/', category_order, name='category_order'),
  path('account/', account_order, name='account_order'),
  path('total/', total_order, name='total_order'),
  # creating paths:
  path('create/', create_receipt, name='create_receipt'),
  path('categories/create/', create_category, name='create_category'),
  path('accounts/create/', create_account, name='create_account'),
  # list paths:
  path('categories/', category_list, name='category_list'),
  path('accounts/', account_list, name='account_list'),
]