from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from receipts.forms import AccountForm, ExpenseCategoryForm, ReceiptForm
from .models import Receipt, Account, ExpenseCategory
# Create your views here.
@login_required
def reciept_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    print("------------------")
    print("receipt total: ", receipt_list[0].total)
    print("receipt tax: ", receipt_list[0].tax)
    print(type(receipt_list[0].total))
    print(type(receipt_list[0].tax))
    print(receipt_list[0].total + receipt_list[0].tax)
    print("------------------")
    context = {
        'receipt_list': receipt_list,
    }
    return render(request, 'receipts/receipts_list.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form.instance.purchaser = request.user
            form.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create_receipt.html', context)

@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'category_list': category_list
    }
    return render(request, 'receipts/category_list.html', context)

@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        'account_list': account_list
    }
    return render(request, 'receipts/account_list.html', context)

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create_category.html', context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create_account.html', context)

@login_required
def vendor_order(request):
        receipt_list = Receipt.objects.filter(purchaser=request.user).order_by('vendor')
        context = {
            'receipt_list': receipt_list,
        }
        return render(request, 'receipts/receipts_list.html', context)

@login_required
def date_order(request):
        receipt_list = Receipt.objects.filter(purchaser=request.user).order_by('date')
        context = {
            'receipt_list': receipt_list,
        }
        return render(request, 'receipts/receipts_list.html', context)

@login_required
def category_order(request):
        receipt_list = Receipt.objects.filter(purchaser=request.user).order_by('category')
        context = {
            'receipt_list': receipt_list,
        }
        return render(request, 'receipts/receipts_list.html', context)

@login_required
def account_order(request):
        receipt_list = Receipt.objects.filter(purchaser=request.user).order_by('account')
        context = {
            'receipt_list': receipt_list,
        }
        return render(request, 'receipts/receipts_list.html', context)

@login_required
def total_order(request):
        receipt_list = Receipt.objects.filter(purchaser=request.user).order_by('total')
        context = {
            'receipt_list': receipt_list,
        }
        return render(request, 'receipts/receipts_list.html', context)